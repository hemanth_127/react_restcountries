import React from 'react'
import './App.css'
import { Routes, Route } from 'react-router-dom'
import { GlobalProvider } from './GlobalContext'
import Heading from './components/Heading'
import Searcher from './components/Searcher'
import Countries from './components/Countries'
import RegionSelector from './components/RegionSelector'
import SubRegionSelector from './components/SubRegionSelector'
import PopulationSorter from './components/PopulationSorter'
import AreaSort from './components/AreaSort'
import CountryDetails from './components/CountryDetails'

function App () {
  return (
    <GlobalProvider>
      <Routes>
        <Route
          path='/'
          element={
            <div className='container'>
              <Heading />
              <section className='searcher'>
                <Searcher />
                <RegionSelector />
                <SubRegionSelector />
                <PopulationSorter />
                <AreaSort />
              </section>
              <section className='grid-section'>
                <Countries />
              </section>
            </div>
          }
        />

        <Route path='/country/:id' element={<CountryDetails />} />
      </Routes>
    </GlobalProvider>
  )
}

export default App
