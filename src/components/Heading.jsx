import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faMoon } from '@fortawesome/free-solid-svg-icons'
import { useState } from 'react'

const Heading = () => {

  const [darkMode, setDarkMode] = useState(false)
  const changeTheme = () => {
    document.body.classList.toggle('dark')
    setDarkMode(!darkMode)
  }

  return (
    <header className='header'>
      <div className='navbar-brand'>
        <a className='navbar-brand' href='#'> Where in the world </a>
      </div>
      
      <div className='icon'>
        <FontAwesomeIcon icon={faMoon} />
        <button className='navbar-btn' onClick={changeTheme}> Dark Mode </button>
      </div>
    </header>
  )
}

export default Heading
