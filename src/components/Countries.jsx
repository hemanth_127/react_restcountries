import React, { useContext, useEffect } from 'react';
import { GlobalContext } from '../GlobalContext';
import { useNavigate } from 'react-router-dom';

const Countries = () => {
  const navigate = useNavigate();
  const { filterData, setDetailview,searchTerm } = useContext(GlobalContext);

  function handleDetailView (country) {
    setDetailview(country.ccn3)
    navigate(`country/${country.ccn3}`)
  }

  if (!filterData) {
    return <p>Loading...</p>;
  }

  return (
    <>
      {filterData.length === 0 ? (
        <p style={{
          color: 'red',
          marginTop: '6rem',
          fontSize: '2rem',
          width: '80vw',
          display: 'flex',
          justifyContent: 'center'
        }}>
           Not Found {searchTerm}
        </p>
      ) : (
        filterData.map(country => {
          const {
            name: { common: name },
            population,
            region,
            capital,
            cca3,
            flags: { png: flags }
          } = country;
          return (
            <article
              key={cca3}
              className='article'
              onClick={() => handleDetailView(country)}
            >
              <img
                className='image'
                src={flags}
                alt={name}
                style={{ width: '100%', height: 200 }}
              />
              <div className='article-div'>
                <h3>{name}</h3>
                <p>
                  Population: <span>{population}</span>
                </p>
                <p>
                  Region: <span>{region}</span>
                </p>
                <p>
                  Capital: <span>{capital}</span>
                </p>
              </div>
            </article>
          );
        })
      )}
    </>
  );
};

export default Countries;
