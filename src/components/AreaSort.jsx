import React, { useContext } from 'react';
import { GlobalContext } from '../GlobalContext'; // Adjust the path accordingly

const AreaSort = () => {
  const { setSortByArea,setSortByPopulation} = useContext(GlobalContext);

  // Handler for area sorting
  const handleAreaSort = event => {
    setSortByPopulation('')
    setSortByArea(event.target.value);
  };

  return (
    <div>
      Sort Area
      <select name='area' id='hel' onChange={handleAreaSort}>
        <option value=''>None</option>
        <option value='Asc1'>Ascending</option>
        <option value='Desc1'>Descending</option>
      </select>
    </div>
  );
};

export default AreaSort;

