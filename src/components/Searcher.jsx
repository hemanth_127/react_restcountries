import React, { useContext } from 'react';
import { Link } from 'react-router-dom';

import { GlobalContext } from '../GlobalContext' // Adjust the path accordingly

const Searcher = () => {
  const { searchTerm, setSearchTerm } = useContext(GlobalContext);

  const handleSearch = event => {
    setSearchTerm(event.target.value.toLowerCase());
  };

  return (
    <>
      <div>
        <input type='text' placeholder='search by country' value={searchTerm} onChange={handleSearch} />
        <Link to='/'></Link>
      </div>
    </>
  )
}

export default Searcher