import React, { useContext } from 'react';
import { GlobalContext } from '../GlobalContext'; // Adjust the path accordingly

const PopulationSorter = () => {
  const { setSortByPopulation,setSortByArea } = useContext(GlobalContext);

  const handlePopulationSort = event => {
    setSortByArea('')
    setSortByPopulation(event.target.value);
  };

  return (
    <div>
      Sort Population
      <select name='population' id='hel' onChange={handlePopulationSort}>
        <option value=''>None</option>
        <option value='Asc'>Ascending</option>
        <option value='Desc'>Descending</option>
      </select>
    </div>
  );
};

export default PopulationSorter;


// import React from 'react'

// const PopulationSorter = ({setSortByPopulation }) => {

//   const handlePopulationSort = event => {
//     setSortByPopulation(event.target.value)
//   }

//   return (
//     <div>
//       Sort Population
//       <select name='population' id='hel' onChange={handlePopulationSort}>
//         <option value=''>None</option>
//         <option value='Asc'>Ascending</option>
//         <option value='Desc'>Descending</option>
//       </select>
//     </div>
//   )
// }

// export default PopulationSorter
