import React, { useContext } from 'react';
import { GlobalContext } from '../GlobalContext'; // Adjust the path accordingly

const SubRegionSelector = () => {
  const { countries, selectedRegion, selectedSubRegion, setSelectedSubRegion } = useContext(GlobalContext);

  // Function to get subregions based on selected region
  const subregions = () => {
    let subRegion = [];
    countries.forEach(country => {
      if (country.region === selectedRegion && country.subregion && !subRegion.includes(country.subregion)) {
        subRegion.push(country.subregion);
      }
    });
    return subRegion.sort();
  };

  // Handler for subregion change
  const handleSubRegionChange = event => {
    setSelectedSubRegion(event.target.value);
  };

  return (
    <select onChange={handleSubRegionChange} value={selectedSubRegion}>
      <option value='' disabled>
        Select Subregion
      </option>
      <option value=''>All SubRegions</option>
      {subregions().map((subregion, index) => (
        <option key={index} value={subregion}>{subregion}</option>
      ))}
    </select>
  );
};

export default SubRegionSelector;



// import React from 'react'

// const SubRegionSelector = ({countries,selectedRegion,selectedSubRegion,setSelectedSubRegion}) => {

//   //subregions
//   const subregions = () => {
//     let subRegion = []
//     countries.forEach(country => {
//       if (country.region === selectedRegion && country.subregion && !subRegion.includes(country.subregion)) {
//            subRegion.push(country.subregion)
//         }
//     })
//     return subRegion.sort()
//   }
  

//   const handleSubRegionChange = event => {
//     setSelectedSubRegion(event.target.value )
//   }

//   return (
//     <select onChange={handleSubRegionChange} value={selectedSubRegion}>
//       <option value='' disabled>
//         Select Subregion
//       </option>
//       <option value=''>All SubRegions</option>
//       {subregions().map((subregion, index) => (
//         <option key={index} value={subregion}>{subregion}</option>
//       ))}
//     </select>
//   )
// }

// export default SubRegionSelector
