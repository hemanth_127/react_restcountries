import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons'
import { GlobalContext } from '../GlobalContext'
import { useContext } from 'react'
import '../App.css'
import Heading from './Heading'
import { useNavigate } from 'react-router-dom'

const CountryDetails = () => {
  const navigate = useNavigate()
  const { detailview, countries } = useContext(GlobalContext)

  const details = countries.filter(country => {
    return country.ccn3 === detailview
  })
  const {
    name,
    population,
    region,
    capital,
    subregion,
    currencies,
    languages,
    flags: { png: flags },
    borders
  } = details[0]

  return (
    <>
      <Heading />
      <div
        className='detail-btn'
        onClick={() => {
          navigate(-1)
        }}
      >
        <FontAwesomeIcon icon={faArrowLeft} />
        <button className='navbar-btn'>Back</button>
      </div>

      <div className='detail'>
        <div>
          <img
            className='image'
            src={flags}
            alt='flag'
            style={{
              width: '30vw',
              height: 300
            }}
          />
        </div>

        <article className='detail-articles'>
          <div className='detail-article'>
            <div className='detail-article-div'>
              <h3>{name.common}</h3>

              <p>
                nativeName: &nbsp;
                {name?.nativeName?.tur?.common ||
                  name?.nativeName?.eng?.common ||
                  'not available'}
              </p>

              <p>
                Population: <span>{population}</span>
              </p>
              <p>
                Region: <span>{region}</span>
              </p>
              <p>subRegion: {subregion}</p>

              <p>
                Capital: <span>{capital}</span>
              </p>
            </div>
            <div className='detail-article-div art'>
              <p>
                Currencies: &nbsp;
                {currencies
                  ? Object.keys(currencies).map(currency => (
                      <span key={currency}>
                        {currencies[currency].name} ({currency})
                      </span>
                    ))
                  : ''}
              </p>
              <p>
                Languages: &nbsp;
                {languages ? Object.values(languages).join(', ') : ''}
              </p>
            </div>
          </div>
          <div className='detail-article2'>
            <p>
              borders :&nbsp;
              {borders
                ? borders.map((border, index) => (
                    <button key={index}>{border}</button>
                  ))
                : 'not available'}
            </p>
          </div>
        </article>
      </div>
    </>
  )
}

export default CountryDetails
