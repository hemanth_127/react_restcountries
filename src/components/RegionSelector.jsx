import React, { useContext } from 'react'
import { GlobalContext } from '../GlobalContext'

const RegionSelector = () => {
  const { countries, selectedRegion, setSelectedRegion, setSelectedSubRegion } =
    useContext(GlobalContext)

  const getRegions = () => {
    let regions = []

    countries.forEach(country => {
      if (country.region && !regions.includes(country.region)) {
        regions.push(country.region)
      }
    })
    return regions.sort()
  }

  const handleRegionChange = event => {
    setSelectedRegion(event.target.value)
    setSelectedSubRegion('')
  }

  return (
    <select onChange={handleRegionChange} value={selectedRegion}>
      <option value='select' disabled>
        Select Region
      </option>
      <option value=''>ALL Regions </option>
      {getRegions().map((region, index) => (
        <option key={index} value={region}>
          {region}
        </option>
      ))}
    </select>
  )
}

export default RegionSelector
