import React, { createContext, useState, useEffect } from 'react'
import { Link } from 'react-router-dom'

export const GlobalContext = createContext()

export const GlobalProvider = ({ children }) => {
  const [countries, setCountries] = useState([])
  const [searchTerm, setSearchTerm] = useState('')
  const [selectedRegion, setSelectedRegion] = useState('')
  const [selectedSubRegion, setSelectedSubRegion] = useState('')
  const [sortByPopulation, setSortByPopulation] = useState('')
  const [sortByArea, setSortByArea] = useState('')
  const [error, setError] = useState('')
  const [filterData, setFilterData] = useState([])
  const [detailview, setDetailview] = useState('')

  useEffect(() => {
    const fetchCountries = async () => {
      try {
        const response = await fetch('https://restcountries.com/v3.1/all')
        if (!response.ok) {
          throw new Error("Couldn't fetch countries data")
        }
        const data = await response.json()
        setCountries(data)
      } catch (error) {
        setError(error.message)
      }
    }
    if (!countries) {
      return (
        <h1 style={{ fontSize: '200px', textAlign: 'center' }}>Loading...</h1>
      )
    }

    fetchCountries()
  }, [])

  const filterCountry = country => {
    const {
      name: { common: name },
      region,
      subregion
    } = country

    const searchMatch = name.toLowerCase().includes(searchTerm.toLowerCase())
    const regionMatch = selectedRegion ? region === selectedRegion : true
    const subRegionMatch = selectedSubRegion
      ? subregion === selectedSubRegion
      : true
    return searchMatch && regionMatch && subRegionMatch
  }

  useEffect(() => {
    setFilterData(countries.filter(filterCountry))
  }, [countries, searchTerm, selectedRegion, selectedSubRegion])

  useEffect(() => {
    const sortCountriesByPopulation = (a, b) => {
      return sortByPopulation === 'Asc'
        ? a.population - b.population
        : b.population - a.population
    }
    const sortCountriesByArea = (a, b) => {
      return sortByArea === 'Asc1' ? a.area - b.area : b.area - a.area
    }

    if (sortByArea) {
      setFilterData(filterData.slice().sort(sortCountriesByArea))
    }

    if (sortByPopulation) {
      setFilterData(filterData.slice().sort(sortCountriesByPopulation))
    }
  }, [sortByPopulation, sortByArea])

  return (
    <GlobalContext.Provider
      value={{
        countries,
        setCountries,
        searchTerm,
        setSearchTerm,
        selectedRegion,
        setSelectedRegion,
        selectedSubRegion,
        setSelectedSubRegion,
        sortByPopulation,
        setSortByPopulation,
        sortByArea,
        setSortByArea,
        error,
        setError,
        filterData,
        detailview,
        setDetailview
      }}
    >
      {children}
    </GlobalContext.Provider>
  )
}
